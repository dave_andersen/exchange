package coinchoose

import (
	"exchange"
	"strings"
	"time"
)

const (
	COINCHOOSE_API_URL          = "http://www.coinchoose.com/api.php?base=BTC"
)

type CoinchooseEntry struct {
	Symbol string `json:"symbol"`
	Difficulty float64 `json:"difficulty,string"`
	Reward float64 `json:"reward,string"`
}

type CoinchooseResponse []CoinchooseEntry

func GetDifficulties() (map[string]CoinchooseEntry, error) {
	var coindat CoinchooseResponse
	// Coinchoose gives bogus replies every now and then;
	// retry once.
	for i := 0; i < 2; i++ {
		err := exchange.DoJSONQuery(COINCHOOSE_API_URL, &coindat)
		if i == 1 && err != nil {
			return nil, err
		}
		if err != nil {
			time.Sleep(500*time.Millisecond)
		} else {
			break
		}
	}
	ret := make(map[string]CoinchooseEntry)
	for _, ent := range coindat {
		ret[strings.ToLower(ent.Symbol)] = ent
	}
	return ret, nil
}


package poloniex

import (
	"bitbucket.org/dave_andersen/exchange"
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	POLONIEX_QUERY_API = "https://poloniex.com/public?command="
	POLONIEX_API       = "https://poloniex.com/tradingApi"

)

type Poloniex struct {
	apikey        string
	apisecret     string
	lastNonceUnix int64
	lastNonceAdd  int64
}

func New(apikey string, apisecret string) *Poloniex {
	return &Poloniex{apikey, apisecret, 0, 0}
}

func currencyPair(sourceCurrency string, destCurrency string) string {
	// Polo:  destCurrency_sourceCurrency, e.g., BTC_BBR
	return strings.ToUpper(destCurrency) + "_" + strings.ToUpper(sourceCurrency)
}

type poloOrderBook struct {
	Asks [][]float64 `json:"asks"`
	Bids [][]float64 `json:"bids"`
}

func GetOrderBook(sourceCurrency string, destCurrency string) (error, *exchange.OrderBook) {
	var resp poloOrderBook
	err := exchange.DoJSONQuery(fmt.Sprintf("%s%s&currencyPair=%s", POLONIEX_QUERY_API,
		"returnOrderBook", currencyPair(sourceCurrency, destCurrency)),
		&resp)
	if err != nil {
		return err, nil
	}
	var ob exchange.OrderBook
	ob.Source = sourceCurrency
	ob.Dest = destCurrency
	for _, ask := range resp.Asks {
		ob.Asks = append(ob.Asks, exchange.Order{ask[0], ask[1]})
	}
	for _, ask := range resp.Bids {
		ob.Bids = append(ob.Bids, exchange.Order{ask[0], ask[1]})
	}
	return nil, &ob
}

type GetBalancesReply struct {
	Stuff int // xxx
}

func (p *Poloniex) GetBalances(balanceMap map[string]float64) error {
	cmdName := "returnBalances"
	var rep map[string]string // GetBalancesReply

	err := p.doAuthRequest(&rep, cmdName, map[string]string{})
	if err != nil {
		fmt.Printf("Coud not get balances: ", err)
		return err
	}

	/* This error handling should be moved into doAuthRequest */
	if rep["error"] != "" {
		return errors.New(rep["error"])
	}
	for k, v := range rep {
		f, err := strconv.ParseFloat(v, 64)
		if err == nil {
			balanceMap[k] = f
		}
	}

	return nil
}

func (p *Poloniex) nextNonce() int64 {
	tUnix := time.Now().Unix() * 1000
	if tUnix > p.lastNonceUnix {
		p.lastNonceUnix = tUnix
		p.lastNonceAdd = 0
	}
	nonce := p.lastNonceUnix + p.lastNonceAdd
	p.lastNonceAdd++
	return nonce
}

func (p *Poloniex) doAuthRequest(resp interface{}, cmdName string, params map[string]string) error {
	params["command"] = cmdName
	params["nonce"] = fmt.Sprintf("%d", p.nextNonce())
	values := url.Values{}
	for k, v := range params {
		values.Add(k, v)
	}
	postdata := values.Encode()

	mac := hmac.New(sha512.New, []byte(p.apisecret))
	mac.Write([]byte(postdata))
	sign := hex.EncodeToString(mac.Sum(nil))
	headers := map[string]string{"Sign": sign,
		"Key": p.apikey}

	err := exchange.DoJSONPostQuery(POLONIEX_API, resp, postdata, headers)
	return err
}


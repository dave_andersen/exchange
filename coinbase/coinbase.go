package coinbase

import (
	"exchange"
	"errors"
)

const (
	COINBASE_PRICE_URL = "https://coinbase.com/api/v1/prices/sell"
)

type AmountInCurrency struct {
	Amount   float64 `json:"amount,string"`
	Currency string  `json:"currency"`
}

type CoinbasePriceDataResponse struct {
	Subtotal AmountInCurrency `json:"subtotal"`
	//	Fees     []map[string]AmountInCurrency `json:"fees"`
	Fees  []interface{}    `json:"fees"` // skipping over this for now
	Total AmountInCurrency `json:"total"`
	AmountInCurrency
}

// Coinbase only provides BTC to USD
func GetPrice(sourceCurrency string, destCurrency string) (float64, error) {

	if (sourceCurrency != "btc" || destCurrency != "usd") &&
		(sourceCurrency != "usd" || destCurrency != "btc") {
		return 0.0, errors.New("unsupported currency pair")
	}

	var coindat CoinbasePriceDataResponse
	err := exchange.DoJSONQuery(COINBASE_PRICE_URL, &coindat)
	if err != nil {
		return 0.0, err
	}
	return coindat.Total.Amount, nil
}


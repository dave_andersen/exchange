package cryptsy

import (
	"exchange"
	"fmt"
	"strings"
	"encoding/hex"
	"crypto/hmac"
	"crypto/sha512"
	"time"
)

const (
	CRYPTSY_PUB_API_URL         = "http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=%s" // % is market ID.  3 for LTC/BTC -- XXX, fix this ugly hack.  Don't hardcode.
	CRYPTSY_MARKET_WDC_BTC      = 14
	CRYPTSY_MARKET_LTC_BTC      = 3
	CRYPTSY_API_URL             = "https://www.cryptsy.com/api"
	CRYPTSY_PUBKEY              = "a2d6905f2d636c00dfe0832682f970c546295816"
	CRYPTSY_PRIVKEY             = "085ad30e55b57be9af8d50ba6db699a75a89ce52c8f806c49f85329e8306d9705b8e73abd1631138"
)

type CryptsyMarketData struct {
	Marketid      string  `json:"marketid"`
	Label         string  `json:"label"`
	Last          float64 `json:"lasttradeprice,string"`
	Primarycode   string  `json:"primarycode"`
	Secondarycode string  `json:"secondarycode"`
	// more that I'm ignoring
}

type CryptsyMarketResponse struct {
	Success int `json:"success"`
	Return  struct {
		Markets map[string]CryptsyMarketData `json:"markets"`
	} `json:"return"`
}

func CryptsyQueryURL(sourceCurrency string, destCurrency string) string {
	// XXX:  Hardcode hack for testing
	marketId := 0
	if sourceCurrency == "ltc" && destCurrency == "btc" {
		marketId = CRYPTSY_MARKET_LTC_BTC
	} else if sourceCurrency == "wdc" && destCurrency == "btc" {
		marketId = CRYPTSY_MARKET_WDC_BTC
	} else {
		fmt.Printf("Error, unknown market: %s - %s\n", sourceCurrency, destCurrency)
		return ""
	}
	return fmt.Sprintf(CRYPTSY_PUB_API_URL, fmt.Sprintf("%d", marketId))
	//	return fmt.Sprintf(CRYPTSY_PUB_API_URL, string.ToUpper(sourceCurrency), string.ToUpper(destCurrency))
}

func GetPrice(sourceCurrency string, destCurrency string) (float64, error) {
	var cryptsydat CryptsyMarketResponse
	err := exchange.DoJSONQuery(CryptsyQueryURL(sourceCurrency, destCurrency), &cryptsydat)
	if err != nil {
		return 0.0, err
	}
	// This is fragile...
	return cryptsydat.Return.Markets[strings.ToUpper(sourceCurrency)].Last, nil
}

func CryptsyAuthReq(postdata string) error {
	// request header variables:
	// Key - pk
	// Sign -
	// nonce - must be incrementing.  let's use unix time minus today's date
	// for this as a starting point
	// HMAC-sha512:  import crypto/sha512  const SHA512
	binkey, err := hex.DecodeString(CRYPTSY_PRIVKEY)
	if err != nil {
		return err
	}
	mac := hmac.New(sha512.New, binkey)
	mac.Write([]byte(postdata))
	sig := mac.Sum(nil)
	fmt.Println("Signature of authreq:")
	fmt.Println(hex.EncodeToString(sig))
	nonce := (time.Now().Unix() - 1386035529)
	fmt.Println("Nonce: ", nonce)
	// set headers
	return nil
}

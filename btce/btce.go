package btce

import (
	"exchange"
	"fmt"
)

const (
	BTCE_TICKER_STRING = "https://btc-e.com/api/2/%s_%s/ticker"
	// %s and %s should be the from/to currencies
)


type BTCTickerResponse struct {
	Ticker struct {
		High       float64 `json:"high"`
		Low        float64 `json:"low"`
		Avg        float64 `json:"avg"`
		Vol        float64 `json:"vol"`
		VolCur     float64 `json:"vol_cur"`
		Last       float64 `json:"last"`
		Buy        float64 `json:"buy"`
		Sell       float64 `json:"sell"`
		Updated    int64   `json:"updated"`
		ServerTime int64   `json:"server_time"`
	} `json:"ticker"`
}

func BTCeQueryURL(sourceCurrency string, destCurrency string) string {
	return fmt.Sprintf(BTCE_TICKER_STRING, sourceCurrency, destCurrency)
}

func GetPrice(sourceCurrency string, destCurrency string) (float64, error) {
	var btcedat BTCTickerResponse
	err := exchange.DoJSONQuery(BTCeQueryURL(sourceCurrency, destCurrency), &btcedat)
	if err != nil {
		return 0.0, err
	}
	return btcedat.Ticker.Buy, nil
}

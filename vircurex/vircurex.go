package vircurex

import (
	"errors"
	"bitbucket.org/dave_andersen/exchange"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"
	"time"
//	"os"
)

const (
	VIRCUREX_QUERY_API           = "https://api.vircurex.com/api/%s.json?base=%s&alt=%s" // op - from - to
	VIRCUREX_API = "https://api.vircurex.com/api/%s.json?"
	ONE_CURRENCY = "get_info_for_1_currency"
	// oType
	UNRELEASED = 0
	RELEASED = 1
)


// Hacked together API for talking to Vircurex

type Vircurex struct {
	username string
	secmap map[string]string
}

func VircurexQueryURL(op string, sourceCurrency string, destCurrency string) string {
	return fmt.Sprintf(VIRCUREX_QUERY_API, op, strings.ToUpper(sourceCurrency), strings.ToUpper(destCurrency))
}

func New(username string, secwords map[string]string) *Vircurex {
	if val, found := secwords["get_balance"];  found {
		secwords["get_balances"] = val
	}
	return &Vircurex{username: username, 
	secmap: secwords}
}

func (v *Vircurex) GetPrice(sourceCurrency string, destCurrency string) (float64, error) {
	return GetPrice(sourceCurrency, destCurrency)
}
//{"base":"DOGE","alt":"BTC","lowest_ask":"0.00000079","highest_bid":"0.00000078","last_trade":"0.00000078","volume":"93146931.3849555"}

type CurrencyInfo struct {
	LowestAsk float64 `json:"lowest_ask,string"`
	HighestBid float64 `json:"highest_bid,string"`
	LastTrade float64 `json:"last_trade,string"`
	Volume float64 `json:"volume,string"`
}
	
func GetCurrencyInfo(sourceCurrency string, destCurrency string) (c *CurrencyInfo, err error) {
	c = &CurrencyInfo{}
	err = exchange.DoJSONQuery(VircurexQueryURL(ONE_CURRENCY, sourceCurrency, destCurrency), c)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func GetPrice(sourceCurrency string, destCurrency string) (float64, error) {
	c, err := GetCurrencyInfo(sourceCurrency, destCurrency)
	if err != nil {
		return 0.0, err
	}
	return c.LastTrade, nil
}

func GetHighestBid(sourceCurrency string, destCurrency string) (float64, error) {
	c, err := GetCurrencyInfo(sourceCurrency, destCurrency)
	if err != nil {
		return 0.0, err
	}
	return c.HighestBid, nil
}

func (v *Vircurex) authRequestURL(cmd string, tokenComponents []string, args map[string]string) (string, error) {
	now := time.Now().UTC().Format("2006-01-02T15:04:05")
	secword := v.secmap[cmd]

	var randbytes [8]byte
	if _, err := rand.Read(randbytes[:]); err != nil {
		return "", err
	}
	transactionId := hex.EncodeToString(randbytes[:])
	argsjoined := ""

	if (args != nil && tokenComponents != nil) {
		var arglist []string
		for _, k := range tokenComponents {
			arglist = append(arglist, args[k])
		}
		argsjoined = strings.Join(arglist, ";")
	}
	var token []string
	if len(argsjoined) > 0 {
		token = []string{secword, v.username, now, transactionId, cmd, argsjoined}
	} else {
		token = []string{secword, v.username, now, transactionId, cmd}
	}
	tokstring := strings.Join(token, ";")
	//fmt.Printf("tokstring: %s\n", tokstring)
	h := sha256.New()
	h.Write([]byte(tokstring))
	tok := hex.EncodeToString(h.Sum(nil))

	url := fmt.Sprintf(VIRCUREX_API, cmd)
	url_args := map[string]string {
		"account" : v.username,
		"id" : transactionId,
		"token" : tok,
		"timestamp" : now,
	}
	for k, v := range args {
		url_args[k] = v
	}

	for k, v := range url_args {
		url += "&" + k + "=" + v // xxx - should do this right.
	}
	return url, nil
//	fmt.Printf("URL:  %s\n", url)
}

type RequestHeader struct {
	Time time.Time
	timestamp string `json:"timestamp"`
	ResponseStatus int `json:"status"`
}

type Balance struct {
	Total float64 `json:"balance,string"`
	Available float64 `json:"availablebalance,string"`
}

type BalancesReply struct {
	RequestHeader
	Balances map[string]Balance `json:"balances"`
}

type BalanceReply struct {
	RequestHeader
	Balance
}

func (h *RequestHeader) Status() int {
	return h.ResponseStatus
}

type Statusable interface {
	Status() int
}

func (v *Vircurex) doAuthRequest(marshalTo Statusable, cmd string, tokenComponents []string, args map[string]string) (error) {
	return v.doAuthRequestWithMap(marshalTo, nil, cmd, tokenComponents, args)
}

// This hideousness is to cope with the fact that some but not all of
// the Vircurex replies are sent as a nested parameter, and others are
// sent flattened in to the top level as variable numbers/names of 
// map entries (e.g., ReadOrders).

func (v *Vircurex) doAuthRequestWithMap(marshalTo Statusable, respMap *map[string]interface{}, cmd string, tokenComponents []string, args map[string]string) (error) {
	url, err := v.authRequestURL(cmd, tokenComponents, args)
	if err != nil {
		return err
	}
	// if (cmd == "release_order") {
	// 	fmt.Printf("URL: %s\n", url)
	// 	os.Exit(0)
	// }
	if err = exchange.DoJSONQueryMap(url, marshalTo, respMap); err != nil {
		return err
	}
	if (marshalTo.Status() != 0) {
		return errors.New(fmt.Sprintf("Nonzero return status from exchange: %d", marshalTo.Status()))
	}
	return nil
}

type Order struct {
	Id int // manually copied in by ReadOrders, alas.
	Type string `json:"ordertype"`
	Quantity float64 `json:"quantity,string"`
	OpenQuantity float64 `json:"openquantity,string"`
	UnitPrice float64 `json:"unitprice,string"`
	SourceCurrency string `json:"currency1"`
	DestCurrency string `json:"currency2"`
	LastChangeTime string `json:"lastchangedat"` // XXX - stuff me into a Time.
	ReleaseTime string `json:"releasedat"`  // XXX - me too.
}
	

type OrdersReply struct {
	RequestHeader
	Num int `json:"numberorders"`
	Orders []Order
}



// oType should be UNRELEASED or RELEASED

// This is a horribly ugly hack to compensate for the strange API
// that vircurex uses for listing orders:  it splats every order
// as "order-#" into the top-level map.
func (v *Vircurex) ReadOrders(oType int) (orderList []Order, err error) {
	tokenComponents := []string{} // otype is NOT included for some reason. :)
	oTypeString := fmt.Sprintf("%d", oType)
	respMap := make(map[string]interface{})
	var rep OrdersReply
	err = v.doAuthRequestWithMap(&rep, &respMap, "read_orders", tokenComponents, map[string]string{ "otype" : oTypeString})
	if err != nil {
		fmt.Printf("Could not read orders: ", err)
	}
	for k, v := range(respMap) {
		if (strings.HasPrefix(k, "order-")) {
			tmp, e := json.Marshal(v)
			if e != nil {
				fmt.Println("Marshal error on order", e)
				continue
			}
			var o Order
			e = json.Unmarshal(tmp, &o)
			if e != nil {
				fmt.Println("Unmarshal error on order", e)
				fmt.Println("Source bytes: ", string(tmp))
				continue
			}
			// orderid is not getting translated properly.
			if m, ok := v.(map[string]interface{}); ok {
				if orderid, ok := m["orderid"].(float64); ok {
					o.Id = int(orderid)
				} else {
					fmt.Printf("Could not coerce %v to int\n", m["orderid"])
				}
			} else {
				fmt.Println("Could not coerce to map")
			}
			rep.Orders = append(rep.Orders, o)
		}
	}
	//fmt.Println("Orders: ", rep)
	return rep.Orders, nil
}

func (v *Vircurex) GetBalances() (repmap map [string]Balance, err error) {
	tokenComponents := []string{}

	var rep BalancesReply
	err = v.doAuthRequest(&rep, "get_balances", tokenComponents, nil)
	if err != nil {
		fmt.Printf("Coud not get balances: ", err)
		return
	}

	repmap = make(map[string]Balance)
	for cur, bal := range rep.Balances {
		repmap[strings.ToLower(cur)] = bal
	}
	return
}

func (v *Vircurex) GetBalance(currency string) (b Balance, err error) {
	tokenComponents := []string{"currency"}

	var rep BalanceReply
	currencyName := strings.ToUpper(currency)
	err = v.doAuthRequest(&rep, "get_balance", tokenComponents, map[string]string {
		"currency" : currencyName })
	if err != nil {
		fmt.Printf("Coud not get balance: ", err)
		return
	}
	b = rep.Balance
	return
}

type CreateOrderReply struct {
	RequestHeader
	Orderid int `json:"orderid"`
}

// "BUY" and "SELL" for orderType
func (v *Vircurex) CreateOrder(ordertype string, sourceCurrency string, destCurrency string, amount float64, price float64) (orderid int, err error) {
	sourceCurrency, destCurrency = strings.ToUpper(sourceCurrency), strings.ToUpper(destCurrency)
	tokenComponents := []string{"ordertype", "amount", "currency1", "unitprice", "currency2"}

//YourSecurityWord;YourUserName;Timestamp;ID;create_order;ordertype;amount;currency1;unitprice;currency2

	var rep CreateOrderReply
	err = v.doAuthRequest(&rep, "create_order", tokenComponents, map[string]string {
		"ordertype" : ordertype,
		"amount" : fmt.Sprint(amount),
		"currency1" : sourceCurrency,
		"unitprice" : fmt.Sprint(price),
		"currency2" : destCurrency,
	})

	if err != nil {
		fmt.Printf("Coud not create order ", err)
		return 0, err
	}

//	fmt.Printf("Create order returned: ", rep)
	return rep.Orderid, nil
}

func (v *Vircurex) ReleaseOrder(orderid int) (newOrderId int, err error) {
	var rep CreateOrderReply
	tokenComponents := []string{"orderid"}
	err = v.doAuthRequest(&rep, "release_order", tokenComponents, map[string]string {
		"orderid" : fmt.Sprint(orderid),
	})
	if err != nil {
		fmt.Printf("Could not release order ", err)
		return 0, err
	}
	return rep.Orderid, nil
}


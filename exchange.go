package exchange

// Generic interfaces for dealing with altcoin exchanges.
import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var debug bool = false

func ReadGetQuery(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	response_body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	resp.Body.Close()
	return response_body, nil
}

func ReadPostQueryHeaders(posturl string, data []byte, headers map[string]string) ([]byte, error) {
	rd := bytes.NewReader(data)

	req, err := http.NewRequest("POST", posturl, rd)
	for h, v := range headers {
		req.Header.Add(h, v)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	cli := &http.Client{}
	resp, err := cli.Do(req)

	if err != nil {
		return nil, err
	}
	response_body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}
	return response_body, nil
}

func DoJSONQuery(url string, dest interface{}) error {
	response_body, err := ReadGetQuery(url)
	if err != nil {
		return err
	}
	if debug {
		fmt.Println("JSON response: ", string(response_body))
	}
	err = json.Unmarshal(response_body, &dest)
	if err != nil {
		return err
	}
	return nil
}

func DoJSONQueryMap(url string, dest interface{}, respMap *map[string]interface{}) error {
	response_body, err := ReadGetQuery(url)
	if err != nil {
		return err
	}
	if debug {
		fmt.Println("JSON response: ", string(response_body))
	}
	err = json.Unmarshal(response_body, &dest)
	if err != nil {
		return err
	}
	if respMap != nil {
		err = json.Unmarshal(response_body, &respMap)
		if err != nil {
			return err
		}
	}
	return nil
}

func DoJSONPostQuery(posturl string, dest interface{}, postdata string, headers map[string]string) error {
	response_body, err := ReadPostQueryHeaders(posturl, []byte(postdata), headers)
	if err != nil {
		return err
	}
	err = json.Unmarshal(response_body, &dest)
	if err != nil {
		return err
	}
	return nil
}

type Order struct {
	Price    float64
	Quantity float64
}

type OrderBook struct {
	Source string // e.g., BBR
	Dest   string // e.g., BTC
	Asks   []Order
	Bids   []Order
}

// Not all exchanges differentiate between total and available.
// For those that do not, total === available.
type Balance struct {
	Total     float64
	Available float64
}
